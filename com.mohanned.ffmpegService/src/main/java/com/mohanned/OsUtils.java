/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mohanned;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
 *
 * @author mkz
 */
public final class OsUtils extends ServerResource {

    private static String OS = null;

    public static String getOsName() {
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static boolean isAndroid() {
        return getOsName().startsWith("Linux");
    }

    
    @Get
    public static String findJettyHome() {
        String home = null;
        if (isAndroid()) {
            home = "/sdcard/Jetty";
        } else if (isWindows()) {
            home = System.getProperty("jetty.home");
            if (home == null) {
                home = System.getenv("JETTY_HOME");
                if (home == null) {
                    System.err.println("No jetty home found ! please add -Djetty.home to your excution command.");
                    System.exit(1);
                    return null;
                }
            }
        }
        return home.replace('\\', '/');
    }

    public static String findJettyTempHome() {
        String temp = null;
        if (isAndroid()) {
            temp = findJettyHome() + "/webapps/tmp";
        } else if (isWindows()) {
            temp = findJettyHome() + "/webapps/temp";
        }
        return temp.replace('\\', '/');
    }

}
