package com.mohanned;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class VideoConversionExample extends ServerResource {

    @Get
    public Representation convert() {

        try {
            loadBinary();

            int res = Runtime.getRuntime().exec("chmod -R 0750 data/data/org.mortbay.ijetty").waitFor();

            // I just use the version execution for example instead than real transcoding.
            
            String fCommand = "data/data/org.mortbay.ijetty/data/ffmpeg -version";

            File execffmpeg = new File("data/data/org.mortbay.ijetty/data/ffmpeg");
            execffmpeg.setExecutable(true);

            ProcessBuilder processBuilder = new ProcessBuilder(fCommand);
            final Map<String, String> environment = processBuilder.environment();

            environment.put("LD_LIBRARY_PATH", "data/data/org.mortbay.ijetty/data/");
            
            environment.put("PATH", environment.get("PATH") + ":/data/data/org.mortbay.ijetty/data/");

            Process process = processBuilder.start();
            process.waitFor();

            return new StringRepresentation(".... DONE >>>>>", MediaType.TEXT_PLAIN);

        } catch (Exception ex) {
            Logger.getLogger(VideoConversionExample.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new StringRepresentation(".... NULLLLLLL >>>>>", MediaType.TEXT_PLAIN);
    }

    static void loadBinary() throws Exception {

        File DestinationffmpegFolder = new File("/data/data/org.mortbay.ijetty/data");
        if (!DestinationffmpegFolder.exists()) {
            DestinationffmpegFolder.mkdir();
        }

        String ffmpegFile = "/data/data/org.mortbay.ijetty/data/ffmpeg"; //x86
        if (!(new File(ffmpegFile)).exists()) {
            copy(OsUtils.findJettyHome() + "/webapps/ffmpegService/web-inf/ffmpeg", ffmpegFile);
        }

        String ffmpegLib = "/data/data/org.mortbay.ijetty/data/libARM_ARCH.so"; //x86
        if (!(new File(ffmpegLib)).exists()) {
            copy(OsUtils.findJettyHome() + "/webapps/ffmpegService/web-inf/libARM_ARCH.so", ffmpegLib);
        }

        System.err.println(" trying to make FFMPEG executable ...");
        (new File(ffmpegLib)).setExecutable(true);

    }

    public static boolean copy(String inputFileName, String outputFileName) {
        FileInputStream is;
        try {
            //must add path web-inf fileNameFromAssets
            is = new FileInputStream(new File(inputFileName));

            // copy ffmpeg file from assets to files dir
            final FileOutputStream os = new FileOutputStream(outputFileName);

            byte[] buffer = new byte[5000];

            int n;
            while (-1 != (n = is.read(buffer))) {
                os.write(buffer, 0, n);
            }

            os.close();
            is.close();

            return true;
        } catch (IOException e) {
            ///
        }

        return false;
    }

}
