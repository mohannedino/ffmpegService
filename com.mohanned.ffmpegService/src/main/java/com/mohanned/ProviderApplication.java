package com.mohanned;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;

public class ProviderApplication extends Application {

    /**
     * Creates a root Restlet that will receive all incoming calls.
     * @return router
     */
    @Override
    public synchronized Restlet createInboundRoot() {
        Router router = new Router(getContext());

        // Defines only one route
        router.attachDefault(new Directory(getContext(), "war:///"));
        router.attach("/who", Who.class);
        router.attach("/convert", VideoConversionExample.class);
        return router;
    }
}
