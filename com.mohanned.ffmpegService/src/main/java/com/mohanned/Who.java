package com.mohanned;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;


public class Who extends ServerResource{

    @Get
    public Representation getMessage() {
        return new StringRepresentation("<tag>I am Service 1 : Type Restlet .. with WADL describtion.. </tag>",
                 MediaType.APPLICATION_XML);
    }
}
